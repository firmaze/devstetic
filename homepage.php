<?php
/**
 * Template name: Homepage
 * The template for displaying Homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Devstetic_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<div class="slideit h-100">
		<div class="slide">
		
		<div class="row">		
		
		<div class="co-xl-12 col-lg-12 col-md-12">
		
		<img src="<?php bloginfo('template_url'); ?>
/img/logo.png"/>
		</div>	
		</div>
		</div>
		
		<div class="slide">
		<div class="row">		
		<div class="co-xl-6 col-lg-6 col-md-6">
		<img src="https://netbramha.com/wp-content/uploads/2016/12/lead-ui-ux-designer-openings.gif"/>
		</div>
		<div class="co-xl-4 col-lg-4 col-md-4">
		<h2>Graphic Design</h2>
		<p>
		"Graphic Design is the work involved in developing a web site for the Internet (World Wide Web) or an intranet (a private network). Web development can range from developing a simple single static page of plain text to complex web-based internet applications (web apps) electronic businesses, and social network services. A more comprehensive list of tasks to which web development commonly refers, may include web engineering, web design, web content development, client liaison, client-side/server-side scripting, web server and network security configuration, and e-commerce development. Among web professionals, "web development" usually refers to the main non-design aspects of building web sites: writing markup and coding. Most recently Web development has come to mean the creation of content management systems (CMS). These CMS can be made from scratch, proprietary or open source. In broad terms the CMS acts as middleware between the database and the user through the browser. A principle benefit of a CMS is that it allows non-technical people to make changes to their web site without having technical knowledge."
		</p>
		</div>	
		</div>
		</div>
		
		<div class="slide">
		<div class="row">		
		<div class="co-xl-6 col-lg-6 col-md-6">
		<img src="https://netbramha.com/wp-content/uploads/2017/02/project-manager-openings.gif"/>
		</div>
		<div class="co-xl-4 col-lg-4 col-md-4">
		<h2>Marketing</h2>
		<p>
		"Marketing is the work involved in developing a web site for the Internet (World Wide Web) or an intranet (a private network). Web development can range from developing a simple single static page of plain text to complex web-based internet applications (web apps) electronic businesses, and social network services. A more comprehensive list of tasks to which web development commonly refers, may include web engineering, web design, web content development, client liaison, client-side/server-side scripting, web server and network security configuration, and e-commerce development. Among web professionals, "web development" usually refers to the main non-design aspects of building web sites: writing markup and coding. Most recently Web development has come to mean the creation of content management systems (CMS). These CMS can be made from scratch, proprietary or open source. In broad terms the CMS acts as middleware between the database and the user through the browser. A principle benefit of a CMS is that it allows non-technical people to make changes to their web site without having technical knowledge."
		</p>
		</div>	
		</div>
		</div>

		</div>

		

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'home' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
