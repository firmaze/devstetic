/**
 * Theme JS Scripts.
 */

(function($) {

    // Slick slider.
    $('.slideit').slick({

    	 slidesToShow: 3,
         slidesToScroll: 3
    });

})(jQuery);